%global _empty_manifest_terminate_build 0
Name:           python-oauthlib
Version:        3.2.2
Release:        3
Summary:        A generic, spec-compliant, thorough implementation of the OAuth request-signing logic
License:        BSD
URL:            https://github.com/oauthlib/oauthlib
Source0:        https://github.com/oauthlib/oauthlib/archive/refs/tags/v%{version}.tar.gz#/oauthlib-%{version}.tar.gz
Patch0:         backport-Update-setup.cfg-to-use-license_files-839.patch
Patch1:         backport-Ensure-expires_at-is-always-int.patch
Patch2:         backport-Use-proper-SPDX-identifier.patch
Patch3:         openEuler-fix-jwt-upgrade-error.patch

BuildArch:      noarch
%description
AuthLib is a framework which implements the logic of OAuth1 or OAuth2
without assuming a specific HTTP request object or web framework. Use
it to graft OAuth client support onto your favorite HTTP library, or
provide support onto your favourite web framework. If you're a
maintainer of such a library, write a thin veneer on top of OAuthLib
and get OAuth support for very little effort.

%package -n python3-oauthlib
Summary:        A generic, spec-compliant, thorough implementation of the OAuth request-signing logic
Provides:       python-oauthlib
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-cryptography
BuildRequires:  python3-blinker
BuildRequires:  python3-cryptography
BuildRequires:  python3-jwt
# General requires
Requires:       python3-cryptography
Requires:       python3-blinker
Requires:       python3-cryptography
Requires:       python3-jwt
%description -n python3-oauthlib
AuthLib is a framework which implements the logic of OAuth1 or OAuth2
without assuming a specific HTTP request object or web framework. Use
it to graft OAuth client support onto your favorite HTTP library, or
provide support onto your favourite web framework. If you're a
maintainer of such a library, write a thin veneer on top of OAuthLib
and get OAuth support for very little effort.

%package help
Summary:        A generic, spec-compliant, thorough implementation of the OAuth request-signing logic
Provides:       python3-oauthlib-doc
%description help
AuthLib is a framework which implements the logic of OAuth1 or OAuth2
without assuming a specific HTTP request object or web framework. Use
it to graft OAuth client support onto your favorite HTTP library, or
provide support onto your favourite web framework. If you're a
maintainer of such a library, write a thin veneer on top of OAuthLib
and get OAuth support for very little effort.

%prep
%autosetup -n oauthlib-%{version} -p1

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-oauthlib -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Aug 27 2024 chenhuihan <chenhuihan@huawei.com> - 3.2.2-3
- fix test error

* Fri may 17 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 3.2.2-2
- Update setup.cfg to use license_files 
- Ensure expires_at is always int
- Use proper SPDX identifier

* Thu Jan 19 2023 Zhipeng Xie <xiezhipeng1@huawei.com> - 3.2.2-1
- Type: requirement
- CVE: NA
- SUG: NA
- DESC: update to 3.2.2

* Mon Sep 26 2022 zhuofeng<zhuofeng2@huawei.com> - 3.2.0-2
- Type:CVE
- CVE:CVE-2022-36087
- SUG:NA
- DESC:fix CVE-2022-36087

* Tue Jul 05 2022 OpenStack_SIG <openstack@openeuler.org> - 3.2.0-1
- Upgrade package python3-oauthlib to version 3.2.0

* Wed May 25 2022 renhongxun <renhongxun@h-partners.com> 3.1.1-2
- Type: requirement
- ID: NA
- SUG: NA
- DESC: remove useless python3-nose from BuildRequires

* Fri Dec 17 2021 renhongxun <renhongxun@huawei.com> - 3.1.1-1
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: upgrade version to 3.1.1

* Mon Sep 27 2021 caodongxia <caodongxia@huawei.com> - 3.1.0-4
- Provides python-oauthlib

* Tue Sep 8 2020 shixuantong <shixuantong@huawei.com> - 3.1.0-3
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: update Source0

* Thu Aug 6 2020 wenzhanli<wenzhanli23@huawei.com> - 3.1.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Remove python2 require and Fix make test

* Thu Jul 23 2020 tianwei <tianwei12@huawei.com> - 3.1.0-1
- Package update to release 3.1.0

* Tue Sep 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-1
- Package init
